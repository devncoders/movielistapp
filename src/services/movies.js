import axios from "axios";
const API_KEY = '906455470c195eb06019fbad97c600dd';
const axiosInstance = axios.create({
    baseURL: 'https://api.themoviedb.org/3',
    timeout: 20000,
});

export function getMovies(page, type) {
    return new Promise(async (resolve, reject) => {
        try {
            const response = await axiosInstance.get(
                `/movie/${type}?api_key=${API_KEY}&page=${page}`
            );
            if (response) resolve(response.data);
            else reject('Something went wrong');
        } catch (error) {
            reject(error?.response?.data);
        }
    });
}

export function getMovieDetails(movieId) {
    return new Promise(async (resolve, reject) => {
        try {
            const response = await axiosInstance.get(
                `/movie/${movieId}?api_key=${API_KEY}&include_image_language=en,null&append_to_response=credits,videos,images`
            );
            if (response) resolve(response.data);
            else reject('Something went wrong');
        } catch (error) {
            reject(error?.response?.data);
        }
    });
}
