import React, { useEffect, useState } from 'react';
import { FlatList, Pressable, ScrollView, StyleSheet, Text, View } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { Colors, Utils } from '../../common';
import { GlobalStyles } from '../../common/styles';
import { Header, ImageView, Spinner } from '../../components';
import { getMovieDetails } from '../../services/movies';
import CastItem from './castItem';
let initialState = {
    isLoading: true,
    readingMore: false,
    detail: {},
    cast: [],
    crew: []
}

const MovieDetail = (props) => {
    const [
        {
            cast,
            crew,
            readingMore,
            isLoading,
            detail
        }, setState
    ] = useState(initialState)
    const movieDetail = props.route.params.item;
    const updateState = (newState) => {
        setState(prevState => ({ ...prevState, ...newState }));
    }
    const _getMovieDetails = () => {
        getMovieDetails(movieDetail.id).then(res => {
            console.log(res.credits.cast);
            updateState({
                detail: res,
                isLoading: false,
                cast: Utils.sliceArrayForLength(res.credits.cast, 12),
                crew: Utils.sliceArrayForLength(res.credits.crew, 12)
            });
        }).catch(err => {
            updateState({ detail: movieDetail, isLoading: false });
        })
    }

    useEffect(() => {
        _getMovieDetails()
    }, []);

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <Header title={movieDetail.original_title} rightButton='Profile' />
            <View style={{ flex: 1, }}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <ImageView
                        resizeMode='contain'
                        style={styles.posterImage}
                        source={Utils.getCharacterImage(detail.poster_path)}
                    />
                    <View style={{ paddingHorizontal: wp(4) }}>
                        <View style={styles.subInfoContainer}>
                            <View style={styles.subInfoSubContainer}>
                                <Text style={styles.subInfoTitle}>Duration</Text>
                                <Text style={styles.subInfoText}>{Utils.convertMinsToHrsMins(detail.runtime)} mins</Text>
                            </View>
                            <View style={styles.separatorVertical} />
                            <View style={styles.subInfoSubContainer}>
                                <Text style={styles.subInfoTitle}>Release</Text>
                                <Text style={styles.subInfoText}>{detail.release_date}</Text>
                            </View>
                            <View style={styles.separatorVertical} />
                            <View style={styles.subInfoSubContainer}>
                                <Text style={styles.subInfoTitle}>Status</Text>
                                <Text style={styles.subInfoText}>{detail.status}</Text>
                            </View>
                        </View>
                        <View style={GlobalStyles.spacer} />

                        <>
                            <Text style={styles.descriptionTitle}>Overview:</Text>
                            <Text style={styles.descriptionText} numberOfLines={readingMore ? null : 3}>{detail.overview}</Text>
                            <Pressable
                                style={styles.readMoreButton}
                                onPress={() => updateState({ readingMore: !readingMore })}>
                                <Text style={styles.readMoreButtonText}>{readingMore ? 'Show Less' : 'Read More'}</Text>
                            </Pressable>
                        </>
                        <>
                            <View style={GlobalStyles.spacer} />
                            <>
                                <Text style={styles.descriptionTitle}>Language:</Text>
                                <Text style={styles.descriptionText}>{Utils.getLanguage(detail.original_language)}</Text>
                            </>
                        </>
                        <View style={GlobalStyles.spacer} />
                        <>
                            <Text style={styles.descriptionTitle}>Cast:</Text>
                            <View style={GlobalStyles.spacer} />
                            <FlatList
                                horizontal
                                data={cast}
                                ItemSeparatorComponent={() => <View style={GlobalStyles.spacerHorizontal} />}
                                extraData={cast}
                                keyExtractor={(item) => item.id}
                                renderItem={CastItem}
                            />
                        </>
                    </View>
                </ScrollView>
            </View>
            {isLoading && <Spinner />}
        </View >
    );
}

const styles = StyleSheet.create({
    posterImage: {
        resizeMode: 'cover',
        width: wp(100),
        height: hp(45),
        alignSelf: 'center'
    },
    subInfoContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: wp(3),
    },
    subInfoSubContainer: {
        flex: 1,
        alignItems: 'center',
        paddingHorizontal: wp(2),
        paddingVertical: hp(1)
    },
    separatorVertical: {
        width: 1,
        backgroundColor: '#eee', marginHorizontal: wp(3)
    },
    subInfoTitle: {
        fontSize: wp(3.5),
        paddingBottom: hp(1),
        color: Colors.textDark
    },
    subInfoText: {
        fontSize: wp(3),
        color: Colors.textLight
    },
    descriptionTitle: {
        color: Colors.headerBg,
        paddingTop: hp(1),
        fontSize: wp(4),
        fontWeight: '600',
    },
    descriptionText: {
        lineHeight: 25,
        paddingTop: hp(1),
        color: Colors.textLight
    },
    readMoreButton: {
        alignSelf: 'flex-end',
    },
    readMoreButtonText: {
        fontSize: wp(3),
        color: Colors.headerBg,
        fontWeight: '500',
        paddingVertical: hp(.5)
    }
})

export default MovieDetail;
