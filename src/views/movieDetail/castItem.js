import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Utils } from '../../common';

const CastItem = ({
    item,
    index
}) => (
    <View style={styles.container}>
        <Image
            accessibilityRole="castimage"
            source={Utils.getCharacterImage(item.profile_path)}
            style={styles.thumbImage}
        />
        <Text numberOfLines={1} style={styles.title}>{item.name || ''}</Text>
    </View>
);

const styles = StyleSheet.create({
    container: {
        padding: wp(2),
        borderWidth: 0.5,
        borderColor: '#eee',
    },
    thumbImage: {
        alignSelf: 'center',
        resizeMode: 'center',
        width: wp(24),
        height: wp(35),
        borderRadius: 5,
    },
    title: {
        fontSize: wp(3),
        paddingVertical: hp(.5)
    }
})

export default CastItem;
