import React from 'react';
import { render } from '@testing-library/react-native';

import CastItem from '../castItem';

const props = {
    item: {
        profile_path: '',
        poster_path: '',
        name: 'Title',
        character: 'Character',
        job: 'Job'
    }
};

test('should verify if has default values in character mode', () => {
    const { getAllByA11yRole, getByText } = render(
        <CastItem {...props} />
    );

    const image = getAllByA11yRole('castimage');
    const name = getByText(props.item.name);
    const character = getByText(props.item.character);

    expect(image).toHaveLength(1);
    expect(name).toBeTruthy();
    expect(character).toBeTruthy();
});

test('should verify if has default values in production mode', () => {
    const { getAllByA11yRole, getByText } = render(
        <PersonRow {...props} />
    );
    const image = getAllByA11yRole('castimage');

    expect(image).toHaveLength(1);
    const name = getByText(props.item.name);
    expect(image).toHaveLength(1);
    expect(name).toBeTruthy();
});

