import React, { useEffect, useState } from 'react';
import { ActivityIndicator, FlatList, Text, View } from 'react-native';
import { GlobalStyles } from '../../common/styles';
import { Header, MovieItem, SearchBar } from '../../components';
import { getMovies } from '../../services/movies';

const Movies = ({ navigation }) => {
    const [searchText, updateSearchText] = useState('');
    const [pageNumber, setPageNumber] = useState(1);
    const [movies, updateMoviesData] = useState([]);
    const [isLoading, setLoading] = useState(true);
    const [loadmore, setLoadMore] = useState(true);
    const [callOnScrollEnd, setcallOnScrollEnd] = useState(false);

    const fetchMovies = () => {
        getMovies(pageNumber, 'popular').then(res => {
            let moviesData = res.results;
            setLoading(false);
            setLoadMore(false);
            if (pageNumber !== 1) {
                moviesData = [...movies, ...moviesData];
            }
            updateMoviesData(moviesData);
        }).catch(err => {
            setLoadMore(false);
            setLoading(false);
        })
    }
    useEffect(() => {
        if (callOnScrollEnd) {
            setLoadMore(true);
            fetchMovies()
        } else {
            fetchMovies()
            setcallOnScrollEnd(false);
        }
    }, [pageNumber]);

    useEffect(() => {
        fetchMovies()
    }, []);

    const getFilteredMovies = () => {
        if (searchText === '') return movies;
        return movies.filter(i => i.title.toLowerCase().includes(searchText.toLowerCase()))
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#fff' }}>
            <Header shouldHideBack={true} title='Movies' />
            <View style={{ flex: 1 }}>
                <SearchBar
                    value={searchText}
                    onChangeText={updateSearchText}
                />
                <View style={{ flex: 1 }}>
                    <FlatList
                        onMomentumScrollEnd={() => { setPageNumber((pageNumber) => pageNumber + 1) }}
                        onEndReached={() => setcallOnScrollEnd(true)}
                        refreshing={isLoading}
                        onRefresh={() => {
                            setPageNumber(1)
                        }}
                        ListEmptyComponent={() => <>
                            {!isLoading && <Text style={GlobalStyles.errorText}>No Movies Found!</Text>}
                        </>}
                        data={getFilteredMovies()}
                        ListFooterComponent={() => <>
                            {!isLoading && loadmore && <ActivityIndicator style={{ marginVertical: 10 }} size={'large'} />}</>}
                        extraData={getFilteredMovies()}
                        ItemSeparatorComponent={() => <View style={GlobalStyles.separator} />}
                        keyExtractor={(item) => item.id}
                        renderItem={(props) => <MovieItem onPress={() => navigation.navigate('MovieDetail', { item: props.item })} {...props} />}
                    />
                </View>
            </View>
        </View>
    );
}

export default Movies;
