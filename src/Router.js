import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Movies from './views/movies';
import MovieDetail from './views/movieDetail';
import PopularMovies from './views/popular';
const Stack = createNativeStackNavigator();
const TabStack = createBottomTabNavigator();

const TabRoutes = () => (
    <TabStack.Navigator
        screenOptions={{
            headerShown: false
        }}
        backBehavior='none'
        tabBarOptions={{

            // showLabel: false,
            /* activeTintColor: Colors.primary,
            inactiveTintColor: Colors.textGrey,
            style: { backgroundColor: Colors.white }, */
        }}
    >
        <Stack.Screen
            name="Latest" component={Movies} />
        <Stack.Screen
            name="Popular"
            component={PopularMovies} />

    </TabStack.Navigator>
)

const AppRoutes = () => {

    useEffect(() => {

    }, []);

    return (
        <NavigationContainer>
            <Stack.Navigator screenOptions={{
                headerShown: false
            }} >
                <Stack.Screen name="Main" component={TabRoutes} />
                <Stack.Screen name="MovieDetail" component={MovieDetail} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default AppRoutes;
