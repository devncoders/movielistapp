import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { Image, Pressable, Text, View } from 'react-native';
import { GlobalStyles } from '../../common/styles';

const Header = (props) => {
    const navigation = useNavigation();
    return (
        <View style={GlobalStyles.headerbar}>
            <View style={GlobalStyles.headerButtonContainer}>
                {navigation.canGoBack() && !props.shouldHideBack && <Pressable onPress={() => navigation.goBack()}>
                    <Image style={GlobalStyles.backArrow} source={require('../../assets/back_arrow.png')} />
                </Pressable>}
            </View>
            <View style={{ flex: 1 }}>
                <Text numberOfLines={1} style={GlobalStyles.headerTitle}>{props.title}</Text>
            </View>
            <View style={GlobalStyles.headerButtonContainer}>
                {props.rightButtonTitle && <Pressable onPress={() => props.onRightButtonPress && props.onRightButtonPress()}>
                    <Text style={GlobalStyles.headerActionButton}>{props.rightButtonTitle}</Text>
                </Pressable>}
            </View>
        </View>
    )
};
Header.defaultProps = {
    rightButtonTitle: null,
    shouldHideBack: false,
    onRightButtonPress: null
}

export { Header };
