import React from 'react';
import { render, fireEvent } from '@testing-library/react-native';
import { SearchBar } from '../searchBar';

test('should verify text in placeholder', () => {
    const { getByA11yRole } = render(<SearchBar />);
    const input = getByA11yRole('search');
    expect(input).toBeTruthy();
});

test('search bar should change value', () => {
    const { getByPlaceholderText, getByDisplayValue } = render(<SearchBar />);
    const input = getByPlaceholderText(/Search/i);
    fireEvent.changeText(input, 'Test');
});