import React from 'react';
import { render } from '@testing-library/react-native';
import { MovieItem } from '../movieItem';

const props = {
    item: {
        poster_path: '',
        original_language: 'en',
        vote_average: 10,
        genre_ids: '',
        type: '',
        release_date: '',
        isSearch: false,
        title: 'Title'
    },
};

test('should verify Movie Item is rendered with props', () => {
    const { getByA11yRole, getByText } = render(<MovieItem {...props} />);
    expect(getByA11yRole('movieimage')).toBeTruthy();
    expect(getByText('Title')).toBeTruthy();
    expect(getByText('10')).toBeTruthy();
});