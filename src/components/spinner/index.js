import React from 'react';
import { ActivityIndicator, View, Dimensions, StyleSheet, Text } from 'react-native';

const Spinner = () => {
    return (
        <View style={[styles.spinnerStyle, StyleSheet.absoluteFill]}>
            <ActivityIndicator size={'large'} />
        </View>
    );
};

const styles = StyleSheet.create({
    spinnerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: "absolute",
        zIndex: 9999,
        backgroundColor: 'rgba(255, 255,255, .6)',
    }
});

export { Spinner };