import * as React from 'react';
import {
  Image,
  ImageProps,
  StyleSheet,
  ImageBackground,
  Text,
} from 'react-native';

const ImageView = (props) => {
  const [isLoaded, setIsLoaded] = React.useState(false);
  const [isError, setError] = React.useState(false);
  return (
    <>
      <ImageBackground
        onError={() => {
          setError(true);
        }}
        onLoadEnd={() => {
          setIsLoaded(true);
        }}
        onLoadStart={() => setIsLoaded(false)}
        source={props.source}
        resizeMode={props.resizeMode}
        {...props}
        style={[props.style, { position: 'relative' }]}>
        {props.source === '' || !props.source || !isLoaded || (isLoaded && isError) ? (
          <>
            <Image
              resizeMode='cover'
              style={[
                styles.image,
                props.style,
                { resizeMode: 'cover', position: 'absolute', zIndex: 9 },
              ]}
              source={require('../../assets/placeholder.jpg')}
            />
          </>
        ) : null}
      </ImageBackground>
    </>
  );
};
ImageView.defaulProps = {
  resizeMode: 'cover',
  placeholderImage: '',
  style: {
    width: 0,
    height: 0,
  },
};

export { ImageView };

const styles = StyleSheet.create({
  image: {
  },
});
