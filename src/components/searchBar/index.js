import React, { useState } from 'react';
import { TextInput, View, StyleSheet } from 'react-native';

const SearchBar = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.subContainer}>
                <TextInput
                    {...props}
                    accessibilityRole="search"
                    placeholderTextColor={'#ccc'}
                    placeholder='Search'
                    style={{ ...styles.input, ...props.style }}
                />
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    input: {
        paddingHorizontal: 12,
        height: 45,
        color: 'black'
    },
    container: {
        backgroundColor: '#eee',
        padding: 15
    },
    subContainer: {
        borderWidth: 0.5,
        borderColor: '#ddd',
        backgroundColor: '#fff',
        borderRadius: 50,
    },
})

export { SearchBar };
