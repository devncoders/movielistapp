import React from 'react';
import { Text, StyleSheet, View, Pressable } from 'react-native';
import { ImageView } from '..';
import { Colors, Utils } from '../../common';
import moment from 'moment';

const MovieItem = (props) => {
    return (
        <Pressable onPress={() => props.onPress()} style={[styles.container, props.index % 2 !== 0 && { backgroundColor: 'rgba(39, 174, 96, 0.1)' }]}>


            <View>
                <ImageView
                    accessibilityRole={'movieimage'}
                    style={styles.imageView}
                    source={Utils.getCharacterImage(props.item.poster_path)}
                />
                <View style={styles.ratingView}>
                    <Text style={styles.ratingCount}>{props.item.vote_average}</Text>
                </View>
            </View>

            <View style={styles.otherInfo}>
                <Text style={styles.title}>{props.item.title}</Text>
                <Text style={styles.releaseDate}>{moment(props.item.release_date).format('MMM DD, YYYY')} | {Utils.getLanguage(props.item.original_language)}</Text>
            </View>
        </Pressable>
    )
};

const styles = StyleSheet.create({
    otherInfo: {
        paddingHorizontal: 15,
        flex: 1,
    },
    container: {
        flexDirection: 'row',
        paddingVertical: 15,
        paddingHorizontal: 8
    },
    imageView: {
        borderRadius: 4,
        overflow: 'hidden',
        width: 130,
        height: 175,
        // backgroundColor: 'red'
    },
    title: {
        paddingVertical: 4,
        fontSize: 18,
        fontWeight: '600'
    },
    releaseDate: {
        fontSize: 13,
    },
    ratingView: {
        position: 'absolute',
        bottom: 5,
        right: 5,
        width: 38,
        height: 38,
        borderRadius: 38 / 2,
        borderWidth: 1,
        borderColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: Colors.ratingBg
    },
    ratingCount: {
        fontSize: 13,
        color: "#fff"
    }
})

export { MovieItem };
