const BASE_IMAGE_URL = 'https://image.tmdb.org/t/p/';
const languages = require('../data/iso.json');
const Utils = {
    getImageBaseUrlUrl: (type) => {
        switch (type) {
            case 'thumb':
                return BASE_IMAGE_URL + 'w500';
            case 'poster':
                return BASE_IMAGE_URL + 'w1280';
            default:
                return BASE_IMAGE_URL + 'w500';
        }
    },
    getLanguage: (lang) => {
        return languages[lang]
    },
    sliceArrayForLength: (arr, num) => {
        return arr.length > num ? arr.slice(0, num) : arr
    },
    convertMinsToHrsMins: time => {
        let hour = Math.floor(time / 60);
        hour = hour < 10 ? `0${hour}` : hour;
        let minutes = time % 60;
        minutes = minutes < 10 ? `0${minutes}` : minutes;

        return hour && minutes ? `${hour}h ${minutes}m` : 'N/A';
    },
    getCharacterImage: (path, key = 'uri', width = 'w500') => {
        return path ? { [key]: `https://image.tmdb.org/t/p/${width}${path}` } : ''
    }
}

export { Utils }