
const Colors = {
    ratingBg: '#27ae60',
    headerBg: '#16a085',
    white: '#fff',
    textLight: '#898989',
    textDark: '#2c3e50'
}

export { Colors }