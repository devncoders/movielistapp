import { StyleSheet } from "react-native";
import { Colors } from ".";

export const GlobalStyles = StyleSheet.create({
    errorText: {
        textAlign: 'center',
        paddingVertical: 10,
        color: Colors.headerBg,
        fontSize: 23
    },
    spacerHorizontal: {
        width: 10
    },
    spacer: {
        height: 10
    },
    button: {
        backgroundColor: '#c0392b',
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 12
    },
    backArrow: {
        width: 30,
        height: 20,
        resizeMode: 'contain',
        tintColor: '#fff'
    },
    buttonText: {
        color: '#fff'
    },
    headerbar: {
        paddingHorizontal: 20,
        height: 55,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Colors.headerBg,
        justifyContent: 'space-between'
    },
    headerTitle: {
        paddingHorizontal: 2,
        color: '#fff',
        fontSize: 22,
        textAlign: 'center'
    },
    headerActionButton: {
        color: '#fff'
    },
    headerButtonContainer: {
        // alignItems: 'center',
        flex: 0.2,
    },
    separator: {
        height: .5,
        backgroundColor: '#ccc'
    },
    titleInput: {
        height: 45,
        paddingHorizontal: 20,
        flex: 1,
        color: 'black'
    },
    addButton: {
        justifyContent: 'center',
        backgroundColor: '#e74c3c',
        paddingHorizontal: 20,
        height: 45
    },
    emptyText: {
        paddingVertical: 8,
        textAlign: 'center',
        color: 'black'

    },
    title: {
        color: 'black',
        fontSize: 30,
        textAlign: 'center',
        paddingVertical: 10
    },
    subTitle: {
        color: 'black',
        fontSize: 23,
        paddingBottom: 25
    },
})